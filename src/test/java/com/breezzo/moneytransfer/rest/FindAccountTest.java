package com.breezzo.moneytransfer.rest;

import com.breezzo.moneytransfer.TestConfiguration;
import com.breezzo.moneytransfer.model.Account;
import com.breezzo.moneytransfer.persistence.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by breezzo on 10.08.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
@WebAppConfiguration
public class FindAccountTest {

    private static final String ACCOUNT_NUM = "123";
    private static final int ACCOUNT_BALANCE = 10;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private AccountRepository accountRepositoryMock;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(webApplicationContext).build();

        Account acc = new Account();
        acc.setNumber(ACCOUNT_NUM);
        acc.setBalance(BigDecimal.valueOf(ACCOUNT_BALANCE));

        when(accountRepositoryMock.findByNumber(eq(ACCOUNT_NUM))).thenReturn(Optional.of(acc));
        when(accountRepositoryMock.findByNumber(argThat(not(ACCOUNT_NUM)))).thenReturn(Optional.empty());
    }

    @Test
    public void findAccount_shouldFind() throws Exception {
        mockMvc.perform(get("/account/123"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.number", is(ACCOUNT_NUM)))
                .andExpect(jsonPath("$.balance", is(ACCOUNT_BALANCE)));
    }

    @Test
    public void findAccount_shouldNotFound() throws Exception {
        mockMvc.perform(get("/account/555"))
                .andExpect(status().isNotFound());
    }
}
