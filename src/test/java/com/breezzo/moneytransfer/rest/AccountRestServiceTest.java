package com.breezzo.moneytransfer.rest;

import com.breezzo.moneytransfer.TestConfiguration;
import com.breezzo.moneytransfer.model.Account;
import com.breezzo.moneytransfer.persistence.AccountRepository;
import com.breezzo.moneytransfer.util.TestUtil;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by breezzo on 10.08.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
@WebAppConfiguration
public class AccountRestServiceTest {

    private static final String SRC_ACC_NUM = "123";
    private static final String DST_ACC_NUM = "321";

    private static final double SRC_ACC_START_BALANCE = 15.55;
    private static final double DST_ACC_START_BALANCE = 1.5;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private AccountRepository accountRepositoryMock;

    private Account srcAcc;
    private Account dstAcc;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(webApplicationContext).build();

        srcAcc = new Account();
        dstAcc = new Account();

        srcAcc.setBalance(BigDecimal.valueOf(SRC_ACC_START_BALANCE));
        srcAcc.setNumber(SRC_ACC_NUM);

        dstAcc.setBalance(BigDecimal.valueOf(DST_ACC_START_BALANCE));
        dstAcc.setNumber(DST_ACC_NUM);

        when(accountRepositoryMock.findByNumber(eq(SRC_ACC_NUM))).thenReturn(Optional.of(srcAcc));
        when(accountRepositoryMock.findByNumber(eq(DST_ACC_NUM))).thenReturn(Optional.of(dstAcc));
        when(accountRepositoryMock.findByNumber(
                argThat(not(
                        isOneOf(SRC_ACC_NUM, DST_ACC_NUM))))).thenReturn(Optional.empty());
    }

    @Test
    public void transfer5_shouldOk() throws Exception {
        final double diff = 5;

        mockMvc.perform(post("/account/transfer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        TestUtil.convertObjectToJsonBytes(
                                transferParam(SRC_ACC_NUM, DST_ACC_NUM, BigDecimal.valueOf(diff)))))
                .andExpect(status().isOk());

        Assert.assertTrue(srcAcc.getBalance().compareTo(BigDecimal.valueOf(SRC_ACC_START_BALANCE - diff)) == 0);
        Assert.assertTrue(dstAcc.getBalance().compareTo(BigDecimal.valueOf(DST_ACC_START_BALANCE + diff)) == 0);
    }

    @Test
    public void transferManyTimes_shouldOk() throws Exception {
        final double diff = 1;
        final int iterationCount = 3;
        final byte[] parameter = TestUtil.
                convertObjectToJsonBytes(transferParam(SRC_ACC_NUM, DST_ACC_NUM, BigDecimal.valueOf(diff)));

        for (int i = 0; i < iterationCount; i++) {
            mockMvc.perform(post("/account/transfer")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(parameter))
                    .andExpect(status().isOk());
        }

        Assert.assertTrue(srcAcc.getBalance().compareTo(BigDecimal.valueOf(SRC_ACC_START_BALANCE - diff  * iterationCount)) == 0);
        Assert.assertTrue(dstAcc.getBalance().compareTo(BigDecimal.valueOf(DST_ACC_START_BALANCE + diff  * iterationCount)) == 0);
    }

    @Test
    public void transferInsufficient_shouldFail() throws Exception {
        final double diff = SRC_ACC_START_BALANCE + 10;

        mockMvc.perform(post("/account/transfer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        TestUtil.convertObjectToJsonBytes(
                                transferParam(SRC_ACC_NUM, DST_ACC_NUM, BigDecimal.valueOf(diff)))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", Matchers.notNullValue()));

        Assert.assertTrue(srcAcc.getBalance().compareTo(BigDecimal.valueOf(SRC_ACC_START_BALANCE)) == 0);
        Assert.assertTrue(dstAcc.getBalance().compareTo(BigDecimal.valueOf(DST_ACC_START_BALANCE)) == 0);
    }

    @Test
    public void transferZero_shouldFail() throws Exception {
        mockMvc.perform(post("/account/transfer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        TestUtil.convertObjectToJsonBytes(
                                transferParam(SRC_ACC_NUM, DST_ACC_NUM, BigDecimal.ZERO))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)));

        Assert.assertTrue(srcAcc.getBalance().compareTo(BigDecimal.valueOf(SRC_ACC_START_BALANCE)) == 0);
        Assert.assertTrue(dstAcc.getBalance().compareTo(BigDecimal.valueOf(DST_ACC_START_BALANCE)) == 0);
    }

    @Test
    public void transferUnknown_shouldNotFound() throws Exception {
        mockMvc.perform(post("/account/transfer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        TestUtil.convertObjectToJsonBytes(
                                transferParam("qwerty", DST_ACC_NUM, BigDecimal.ONE))))
                .andExpect(status().isGone());

        Assert.assertTrue(dstAcc.getBalance().compareTo(BigDecimal.valueOf(DST_ACC_START_BALANCE)) == 0);

        mockMvc.perform(post("/account/transfer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        TestUtil.convertObjectToJsonBytes(
                                transferParam(SRC_ACC_NUM, "qwerty", BigDecimal.ONE))))
                .andExpect(status().isGone());

        Assert.assertTrue(srcAcc.getBalance().compareTo(BigDecimal.valueOf(SRC_ACC_START_BALANCE)) == 0);

        mockMvc.perform(post("/account/transfer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        TestUtil.convertObjectToJsonBytes(
                                transferParam("qwerty", "qwerty", BigDecimal.ONE))))
                .andExpect(status().isGone());
    }

    @Test
    public void transferEmpty_shouldFail() throws Exception {
        mockMvc.perform(post("/account/transfer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        TestUtil.convertObjectToJsonBytes(
                                transferParam(null, null, BigDecimal.ONE))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)));

        mockMvc.perform(post("/account/transfer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        TestUtil.convertObjectToJsonBytes(
                                transferParam("", "", BigDecimal.ONE))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    private static TransferParameter transferParam(String sourceAccountNumber, String destinationAccountNumber, BigDecimal amount) {
        TransferParameter result = new TransferParameter();
        result.setSourceAccountNumber(sourceAccountNumber);
        result.setDestinationAccountNumber(destinationAccountNumber);
        result.setAmount(amount);
        return result;
    }
}
