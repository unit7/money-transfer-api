package com.breezzo.moneytransfer.service;

import com.breezzo.moneytransfer.exception.IncorrectTransferException;
import com.breezzo.moneytransfer.model.Account;
import com.breezzo.moneytransfer.persistence.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * @author breezzo
 */
@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public Optional<Account> findAccount(@Nonnull String number) {
        return accountRepository.findByNumber(number);
    }

    /**
     * Переводит сумму с одного счета на другой.
     * @param from Счет, с которого будет выполняться перевод
     * @param to Счет, на который будет выполняться перевод
     * @param amount Сумма перевода
     *
     * @throws NullPointerException если один из параметров null
     * @throws IncorrectTransferException если сумма не положительная или на указанном счете недостаточно средств
     */
    public void transfer(@Nonnull Account from, @Nonnull Account to, @Nonnull BigDecimal amount) {

        checkCorrectAmount(amount);

        Account first = from;
        Account second = to;
        if (first.getNumber().compareTo(second.getNumber()) > 0) {
            first = to;
            second = from;
        }

        synchronized (first) {
            synchronized (second) {
                BigDecimal fromBalance = first.getBalance();
                if (fromBalance.compareTo(amount) < 0) {
                    throw new IncorrectTransferException("Insufficient amount on account " +
                            from.getNumber() + ", requested amount " + amount);
                }

                first.setBalance(fromBalance.subtract(amount));
                second.setBalance(second.getBalance().add(amount));
            }
        }
    }

    private void checkCorrectAmount(BigDecimal amount) {
        if (amount.signum() <= 0) {
            throw new IncorrectTransferException("Only positive amount allowed: " + amount);
        }
    }
}
