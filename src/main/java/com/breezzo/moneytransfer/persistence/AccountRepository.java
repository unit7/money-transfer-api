package com.breezzo.moneytransfer.persistence;

import com.breezzo.moneytransfer.model.Account;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * @author breezzo
 */
@Repository
public interface AccountRepository {
    Optional<Account> findByNumber(@Nonnull String number);
}
