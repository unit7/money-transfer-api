package com.breezzo.moneytransfer.persistence;

import com.breezzo.moneytransfer.model.Account;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by breezzo on 10.08.16.
 */
public class AccountRepositoryStub implements AccountRepository {

    private final Map<String, Account> accountStorage = new HashMap<>();

    public AccountRepositoryStub() {
        accountStorage.put("123456", account("123456", 123.10));
        accountStorage.put("654321", account("654321", 10));
        accountStorage.put("451263", account("451263", 55.20));
        accountStorage.put("451265", account("451265", 15000));
        accountStorage.put("163254", account("163254", 3112));
        accountStorage.put("431526", account("431526", 1000130.10));
    }

    private static Account account(String number, double amount) {
        Account acc = new Account();
        acc.setBalance(BigDecimal.valueOf(amount));
        acc.setNumber(number);
        return acc;
    }

    @Override
    public Optional<Account> findByNumber(@Nonnull String number) {
        return Optional.ofNullable(accountStorage.get(number));
    }
}
