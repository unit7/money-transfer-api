package com.breezzo.moneytransfer;

import com.breezzo.moneytransfer.persistence.AccountRepository;
import com.breezzo.moneytransfer.persistence.AccountRepositoryStub;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * @author breezzo
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(new Class<?>[] { Application.class }, args);
    }

    @Bean
    public AccountRepository accountRepository() {
        return new AccountRepositoryStub();
    }

    @Bean
    public Validator validator() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        return validatorFactory.getValidator();
    }
}
