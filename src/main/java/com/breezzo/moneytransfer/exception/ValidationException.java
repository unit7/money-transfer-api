package com.breezzo.moneytransfer.exception;

import javax.validation.ConstraintViolation;
import java.util.Set;

/**
 * Created by breezzo on 11.08.16.
 */
public class ValidationException extends RuntimeException{

    private Set<ConstraintViolation<?>> violations;

    public ValidationException(Set<ConstraintViolation<?>> violations) {
        this.violations = violations;
    }

    public Set<ConstraintViolation<?>> getViolations() {
        return violations;
    }
}
