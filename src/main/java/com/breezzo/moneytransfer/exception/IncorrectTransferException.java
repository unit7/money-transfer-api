package com.breezzo.moneytransfer.exception;

/**
 * Created by breezzo on 10.08.16.
 */
public class IncorrectTransferException extends RuntimeException {
    public IncorrectTransferException() {
    }

    public IncorrectTransferException(String message) {
        super(message);
    }

    public IncorrectTransferException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectTransferException(Throwable cause) {
        super(cause);
    }

    public IncorrectTransferException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
