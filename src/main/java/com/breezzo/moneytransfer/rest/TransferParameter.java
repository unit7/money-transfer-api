package com.breezzo.moneytransfer.rest;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * Параметры перевода денег с одного счета на другой
 *
 * Created by breezzo on 11.08.16.
 */
public class TransferParameter {
    @NotNull
    @Size(min = 1, max = 6)
    private String sourceAccountNumber;

    @NotNull
    @Size(min = 1, max = 6)
    private String destinationAccountNumber;

    @NotNull
    @DecimalMin("0.01")
    private BigDecimal amount;

    public String getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public TransferParameter setSourceAccountNumber(String sourceAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
        return this;
    }

    public String getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public TransferParameter setDestinationAccountNumber(String destinationAccountNumber) {
        this.destinationAccountNumber = destinationAccountNumber;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public TransferParameter setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TransferAmountParam{");
        sb.append("sourceAccountNumber='").append(sourceAccountNumber).append('\'');
        sb.append(", destinationAccountNumber='").append(destinationAccountNumber).append('\'');
        sb.append(", amount=").append(amount);
        sb.append('}');
        return sb.toString();
    }
}
