package com.breezzo.moneytransfer.rest;

import com.breezzo.moneytransfer.exception.IncorrectTransferException;
import com.breezzo.moneytransfer.exception.ValidationException;
import com.breezzo.moneytransfer.model.Account;
import com.breezzo.moneytransfer.model.ValidatorHelper;
import com.breezzo.moneytransfer.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Предоставляет API для работы со счетами
 *
 * @author breezzo
 */
@RestController
@RequestMapping("/account")
public class AccountRestService {

    private static final Logger logger = LoggerFactory.getLogger(AccountRestService.class);

    @Autowired
    private ValidatorHelper validatorHelper;

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/transfer",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> transfer(@RequestBody TransferParameter parameter) {

        validatorHelper.validate(parameter);

        String accountNumberFrom = parameter.getSourceAccountNumber();
        String accountNumberTo = parameter.getDestinationAccountNumber();
        BigDecimal amount = parameter.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP);

        Optional<Account> accountFrom = accountService.findAccount(accountNumberFrom);
        Optional<Account> accountTo = accountService.findAccount(accountNumberTo);

        if (!accountFrom.isPresent() || !accountTo.isPresent()) {
            return new ResponseEntity<>(HttpStatus.GONE);
        }

        accountService.transfer(accountFrom.get(), accountTo.get(), amount);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{number}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> findByNumber(@PathVariable("number") String number) {
        final Optional<Account> account = accountService.findAccount(number);

        if (!account.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(account.get());
    }

    @ExceptionHandler(IncorrectTransferException.class)
    public ResponseEntity<TransferResponse> incorrectTransferExceptionHandler(IncorrectTransferException ex) {
        logger.error("Error occurred during transfer money: {}", ex.getMessage());
        return ResponseEntity.badRequest().body(new TransferResponse(ex.getMessage()));
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<?> validationExceptionHandler(ValidationException e) {
        final Set<ConstraintViolation<?>> violations = e.getViolations();
        return ResponseEntity.badRequest().body(
                violations.stream().map((v) -> {
                    String propertyPath = v.getPropertyPath().toString();
                    String message = v.getMessage();
                    return new FieldError("transferParameter", propertyPath, "Invalid(" + propertyPath + "): " + message);
                }).collect(Collectors.toList())
        );
    }
}
