package com.breezzo.moneytransfer.model;

import com.breezzo.moneytransfer.exception.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

/**
 * Created by breezzo on 11.08.16.
 */
@Component
public class ValidatorHelper {

    @Autowired
    private Validator validator;

    public <T> void validate(T model, Class<?>... groups) throws ValidationException {
        final Set<ConstraintViolation<T>> violations = validator.validate(model, groups);
        if (!violations.isEmpty()) {
            // noinspection unchecked
            throw new ValidationException((Set) violations);
        }
    }
}
